package com.team.angular.interactiondesignapi.models;

public enum ZahlungMethod {
	
	Einmal, Ratenzahlung, Guthaben, Gutschein, Anzahlung_150;

}
